import {Component, OnInit} from '@angular/core';
import {WeatherService} from '../../services/weather/weather.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  appCities: string[]
  constructor(public weather: WeatherService) {
}

  ngOnInit() {

    this.appCities = this.weather.getAppCiteis()

  }
}